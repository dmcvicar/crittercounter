/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdio.h>
#include "exif.h"
#include "qexif.h"

#include <QDebug>
#include <QUrl>

QExif::QExif(QObject *parent) : QObject(parent)
{
}

QString QExif::creationDate()
{
    return QString(result.DateTimeDigitized.c_str());
}

QString QExif::fileName()
{
    return m_fileName;
}

void QExif::setFileName(const QString &fileName)
{
    if (fileName == m_fileName)
        return;
    m_fileName = fileName;

    m_errorText = "";

    QUrl localName(fileName);
    QByteArray ba = localName.toLocalFile().toLatin1();
    const char *c_str2 = ba.data();

    // Read the JPEG file into a buffer
    FILE *fp = fopen(c_str2, "rb");
    if (fp) {
        fseek(fp, 0, SEEK_END);
        unsigned long fsize = ftell(fp);
        rewind(fp);
        unsigned char *buf = new unsigned char[fsize];
        if (fread(buf, 1, fsize, fp) == fsize) {
            fclose(fp);
            //  Parse EXIF
            int code = result.parseFrom(buf, fsize);
            delete[] buf;
        } else {
            m_errorText = "read failed";
        }
    } else {
        m_errorText = "open failed";
    }
    emit fileNameChanged();
}
