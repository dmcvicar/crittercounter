/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "namemodel.h"

Name::Name(QObject *parent) :
    QObject(parent), m_name("None")
{ }

Name::Name(const QString &name, QObject *parent) :
    QObject(parent), m_name(name)
{ }

QString Name::name() const
{
    return m_name;
}

void Name::setName(const QString &name)
{
    m_name = name;
    emit nameChanged();
}

NameModel::NameModel(QObject *parent) :
    QAbstractListModel(parent)
{ }

void NameModel::clear()
{
    beginResetModel();
    // call delete on each elementin list
    qDeleteAll(m_names.begin(), m_names.end());
    m_names.clear();
    endResetModel();
    emit countChanged(this->rowCount());
}

void NameModel::setRowData(int row, const QString &role, const QVariant &value)
{
    Name *name = m_names[row];
    if (role == "name")
        name->setName(value.toString());
    emit dataChanged(index(row), index(row));
}

QVariant NameModel::getRowData(int row, const QString &role)
{
    Name *name = m_names[row];
    if (role == "name")
        return name->name();
    return QVariant();
}

void NameModel::addName(Name *name)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_names << name;
    endInsertRows();
    emit countChanged(this->rowCount());
}

int NameModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_names.count();
}

QVariant NameModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_names.count())
        return QVariant();

    Name *name = m_names[index.row()];
    if (role == NameRole)
        return name->name();
    return QVariant();
}

QHash<int, QByteArray> NameModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole]  = "name";
    return roles;
}
