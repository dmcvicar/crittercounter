/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef PHOTOMODEL_H
#define PHOTOMODEL_H

#include <QObject>
#include <QList>
#include <QAbstractListModel>

class Photo : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString fileName  READ fileName  WRITE setFileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString photoDate READ photoDate WRITE setPhotoDate NOTIFY photoDateChanged)
    Q_PROPERTY(QString oneType   READ oneType   WRITE setOneType NOTIFY oneTypeChanged)
    Q_PROPERTY(int     oneCount  READ oneCount  WRITE setOneCount NOTIFY oneCountChanged)
    Q_PROPERTY(int     oneDup    READ oneDup    WRITE setOneDup NOTIFY oneDupChanged)
    Q_PROPERTY(QString oneConf   READ oneConf   WRITE setOneConf NOTIFY oneConfChanged)
    Q_PROPERTY(bool    oneReqChk READ oneReqChk WRITE setoneReqChk NOTIFY oneReqChkChanged)
    Q_PROPERTY(QString twoType   READ twoType   WRITE setTwoType NOTIFY twoTypeChanged)
    Q_PROPERTY(int     twoCount  READ twoCount  WRITE setTwoCount NOTIFY twoCountChanged)
    Q_PROPERTY(int     twoDup    READ twoDup    WRITE setTwoDup NOTIFY twoDupChanged)
    Q_PROPERTY(QString twoConf   READ twoConf   WRITE setTwoConf NOTIFY twoConfChanged)
    Q_PROPERTY(bool    twoReqChk READ twoReqChk WRITE setTwoReqChk NOTIFY twoReqChkChanged)

public:
    Photo(QObject *parent = nullptr);

    Photo(const QString &fileName, const QString &photoDate,
          const QString &oneType, const int &oneCount, const int &oneDup, const QString &oneConf, const bool oneReqChk,
          const QString &twoType, const int &twoCount, const int &twoDup, const QString &twoConf, const bool twoReqChk,
          QObject *parent = nullptr);

    QString fileName()  const;
    QString photoDate() const;
    QString oneType() const;
    int     oneCount()  const;
    int     oneDup()    const;
    QString oneConf() const;
    bool    oneReqChk() const;
    QString twoType()   const;
    int     twoCount()  const;
    int     twoDup()    const;
    QString twoConf() const;
    bool    twoReqChk() const;

    void setFileName(const QString &fileName);
    void setPhotoDate(const QString &photoDate);
    void setOneType(const QString &oneType);
    void setOneCount(int oneCount);
    void setOneDup(int oneDup);
    void setOneConf(const QString &oneConf);
    void setoneReqChk(const bool oneReqChk);
    void setTwoType(const QString &twoType);
    void setTwoCount(int twoCount);
    void setTwoDup(int twoDup);
    void setTwoConf(const QString &twoConf);
    void setTwoReqChk(const bool twoReqChk);

signals:
    void fileNameChanged();
    void photoDateChanged();
    void oneTypeChanged();
    void oneCountChanged();
    void oneDupChanged();
    void oneConfChanged();
    void oneReqChkChanged();
    void twoTypeChanged();
    void twoCountChanged();
    void twoDupChanged();
    void twoConfChanged();
    void twoReqChkChanged();

private:
    QString m_fileName;
    QString m_photoDate;
    QString m_oneType;
    int     m_oneCount;
    int     m_oneDup;
    QString m_oneConf;
    bool    m_oneReqChk;
    QString m_twoType;
    int     m_twoCount;
    int     m_twoDup;
    QString m_twoConf;
    bool    m_twoReqChk;
};


class PhotoModel: public QAbstractListModel
{
    Q_OBJECT

public:
    enum PhotoRoles {
        FileNameRole = Qt::UserRole + 1,
        PhotoDateRole,
        OneTypeRole,
        OneCountRole,
        OneDupRole,
        OneConfRole,
        OneReqCheckRole,
        TwoTypeRole,
        TwoCountRole,
        TwoDupRole,
        TwoConfRole,
        TwoReqCheckRole,
    };

    PhotoModel(QObject *parent = nullptr);

    Q_INVOKABLE void addPhoto();
    Q_INVOKABLE void clear();
    Q_INVOKABLE void setRowData(int row, const QString &role, const QVariant &value);
    Q_INVOKABLE QVariant getRowData(int row, const QString &role);

    Q_PROPERTY(int currentIndex READ currentIndex WRITE setCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(QString description READ description WRITE setDescription NOTIFY descriptionChanged)

    int      rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    Photo*   photo(int index);
    int currentIndex() const;
    void setCurrentIndex(int currentIndex);
    QString description() const;
    void setDescription(QString description);

signals :
    void countChanged(int);
    void currentIndexChanged(int);
    void descriptionChanged(QString);

 protected:
     QHash<int, QByteArray> roleNames() const;

 private:
     QList<Photo *> m_photos;
     int m_currentIndex;
     QString m_description;
     QString timeBetween(int i, int j);
     QString critterInfo(int i);
};

#endif // PHOTOMODEL_H
