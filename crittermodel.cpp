/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include "crittermodel.h"

Critter::Critter(QObject *parent) :
    QObject(parent), m_name("??"), m_symbol("??")
{ }

Critter::Critter(const QString &name, const QString &symbol, QObject *parent) :
    QObject(parent), m_name(name), m_symbol(symbol)
{ }

QString Critter::name() const
{
    return m_name;
}

QString Critter::symbol() const
{
    return m_symbol;
}

void Critter::setName(const QString &name)
{
    m_name = name;
    emit nameChanged();
}

void Critter::setSymbol(const QString &symbol)
{
    m_symbol = symbol;
    emit symbolChanged();
}

CritterModel::CritterModel(QObject *parent):
    QAbstractListModel(parent)
{ }

void CritterModel::addCritter(Critter *critter)
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_critters << critter;
    endInsertRows();
    emit countChanged(this->rowCount());
}

void CritterModel::clear()
{
    beginResetModel();
    // call delete on each elementin list
    qDeleteAll(m_critters.begin(), m_critters.end());
    m_critters.clear();
    endResetModel();
    emit countChanged(this->rowCount());
}

void CritterModel::setRowData(int row, const QString &role, const QVariant &value)
{
    Critter *critter = m_critters[row];
    if (role == "name")
        critter->setName(value.toString());
    else if (role == "symbol")
        critter->setSymbol(value.toString());
    emit dataChanged(index(row), index(row));
}

QVariant CritterModel::getRowData(int row, const QString &role)
{
    Critter *critter = m_critters[row];
    if (role == "name")
        return critter->name();
    else if (role == "symbol")
        return critter->symbol();
    return QVariant();
}

int CritterModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return m_critters.count();
}

QVariant CritterModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_critters.count())
        return QVariant();

    Critter *critter = m_critters[index.row()];
    if (role == NameRole)
        return critter->name();
    else if (role == SymbolRole)
        return critter->symbol();
    return QVariant();
}

// ugly -- should use faster search
QString CritterModel::symbol(const QString &name)
{
    for (int i = 0; i < m_critters.count(); i++)
        if (name == m_critters[i]->name())
            return m_critters[i]->symbol();
    return "";
}

// ugly -- should use faster search
QString CritterModel::name(const QString &symbol)
{
    for (int i = 0; i < m_critters.count(); i++)
        if (symbol == m_critters[i]->symbol())
            return m_critters[i]->name();
    return "";
}

QHash<int, QByteArray> CritterModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[NameRole]  = "name";
    roles[SymbolRole] = "symbol";
    return roles;
}
