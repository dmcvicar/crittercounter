#ifndef QEXIF_H
#define QEXIF_H

#include <QObject>
#include "exif.h"
#include "qexif.h"

/*
    Unfortunatly QQickImage does not deal with EXIF data.  So ...
    This is a crap class that is used to get date and time information for a JPG file
    Originaly I looked at several open source exif lifraries and realized that they read the entire
    file into memory. Probaby easier to just use QImage.

    On the other hand, QImage gave an empty list for keys. WTF!!
    TRY easyexif lib

    Copyright (c) 2017-2018 David McVicar
    dmcvicar@telus.net
    All rights reserved (BSD License).

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        -- Redistributions of source code must retain the above copyright notice,
           this list of conditions and the following disclaimer.
        -- Redistributions in binary form must reproduce the above copyright notice,
           this list of conditions and the following disclaimer in the documentation
           and/or other materials provided with the distribution.

        THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESS
        OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
        OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
        NO EVENT SHALL THE FREEBSD PROJECT OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
        INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
        BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
        DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
        OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
        NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
        EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

class QExif : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString fileName READ fileName WRITE setFileName NOTIFY fileNameChanged)
    Q_PROPERTY(QString creationDate READ creationDate)

public:
    explicit QExif(QObject *parent = nullptr);
    QString fileName();
    void setFileName(const QString &fileName);
    QString creationDate();

signals:
    void fileNameChanged();

public slots:

private:
    QString m_fileName; // actualy passing url .. deal with later
    easyexif::EXIFInfo result;
    QString m_errorText;
};

#endif // QEXIF_H
