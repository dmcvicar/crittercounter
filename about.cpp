/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <QFile>
#include <QTextStream>
#include "about.h"

#include <QDebug>

About::About(QObject *parent) :
    QObject(parent), m_help(""), m_licence(""), m_acknowledgment("")
{
    QFile file("://resources/license.html");
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(&file);
        setLicence(in.readAll());
    }
    file.close();
    file.setFileName("://resources/help.html");
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(&file);
        setHelp(in.readAll());
    }
    file.close();
    //acknowledgment
    //Acknowledgment
    file.setFileName("://resources/acknowledgment.html");
    if (file.open(QFile::ReadOnly | QFile::Text)) {
        QTextStream in(&file);
        setAcknowledgment(in.readAll());
    }
    file.close();
}

QString About::help() const
{
    return m_help;
}

QString About::licence() const
{
    return m_licence;
}

QString About::acknowledgment() const
{
    return m_acknowledgment;
}

void About::setHelp(const QString &help)
{
    m_help = help;
    emit helpChanged();
}

void About::setLicence(const QString &licence)
{
    m_licence = licence;
    emit licenceChanged();
}

void About::setAcknowledgment(const QString &acknowledgment)
{
    m_acknowledgment = acknowledgment;
    emit acknowledgmentChanged();
}

