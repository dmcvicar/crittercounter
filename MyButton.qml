import QtQuick 2.0


Item {
    signal clicked
    property alias toolTip : tooltipText.text
    property alias upImage: imageUp.source
    property alias downImage: imageDown.source
    width: 70
    height: 70

    states: [
        State { // this will fade in immageDown and fade out imageUp
            name: "fadeInImageDown"
            PropertyChanges { target: imageUp; opacity: 0}
            PropertyChanges { target: imageDown; opacity: 1}
        },
        State   { // this will fade in immageUp and fade out imageDown
            name:"fadeOutImageDown"
            PropertyChanges { target: imageUp; opacity: 1}
            PropertyChanges { target: imageDown; opacity: 0}
        }
    ]
    transitions: [
        Transition {
            NumberAnimation { property: "opacity"; easing.type: Easing.InOutQuad; duration: 100  }
        }
    ]
    Image {
        id: imageDown
        smooth: true
        anchors.fill: parent
    }
    Image {
        id: imageUp
        smooth: true
        anchors.fill: parent
    }
    state: "fadeOutImageDown"
    MouseArea {
        id: m_area
        anchors.fill: parent
        anchors.margins: -10
        hoverEnabled: true
        onPressed: {
            hoverEnabled: false
            tooltip.visible = false
            parent.state = "fadeInImageDown"
        }
        onReleased: {
            parent.state = "fadeOutImageDown"
            parent.clicked()
        }
        onPositionChanged: {
            tooltip.visible = false
            hoverTimer.start()
        }
        onExited: {
            tooltip.visible = false
            hoverTimer.stop()
        }
        Timer {
            id: hoverTimer
            interval: 1000
            onTriggered: {
                tooltip.x = parent.mouseX + 10
                tooltip.y = parent.mouseY
                tooltip.visible = true
            }
        }
        Rectangle {
            id: tooltip
            border.color: "black"
            color: "light cyan"
            radius:5
            width: tooltipText.implicitWidth + 8
            height: tooltipText.implicitHeight + 8
            visible: false
            Text {
                id: tooltipText
                anchors.centerIn: parent
            }
        }
    }
}
