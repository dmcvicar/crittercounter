/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef NAMEMODEL_H
#define NAMEMODEL_H

#include <QObject>
#include <QList>
#include <QAbstractListModel>

class Name: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    Name(QObject *parent = nullptr);
    Name(const QString &name, QObject *parent = nullptr);

    QString name()  const;
    void setName(const QString &name);

signals:
    void nameChanged();

private:
    QString m_name;
};


class NameModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum NameRoles {
        NameRole = Qt::UserRole + 1,
    };

    NameModel(QObject *parent = nullptr);

    Q_INVOKABLE void clear();
    Q_INVOKABLE void setRowData(int row, const QString &role, const QVariant &value);
    Q_INVOKABLE QVariant getRowData(int row, const QString &role);

    void     addName(Name *name);
    int      rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

signals :
    void countChanged(int);

 protected:
     QHash<int, QByteArray> roleNames() const;

 private:
     QList<Name *> m_names;
};

#endif // NAMEMODEL_H
