/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef MODELSET_H
#define MODELSET_H

#include <QObject>
#include <QXmlStreamWriter>
#include "photomodel.h"
#include "crittermodel.h"
#include "namemodel.h"

class ModelSet : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString transect    READ transect     WRITE setTransect    NOTIFY transectChanged)
    Q_PROPERTY(QString subtransect READ subtransect  WRITE setSubtransect NOTIFY subtransectChanged)
    Q_PROPERTY(QString camera      READ camera       WRITE setCamera      NOTIFY cameraChanged)
    Q_PROPERTY(QString sdcard      READ sdcard       WRITE setSdcard      NOTIFY sdcardChanged)
    Q_PROPERTY(QString analyst     READ analyst      WRITE setAnalyst     NOTIFY analystChanged)
    Q_PROPERTY(QString copyAndRenameMessage READ copyAndRenameMessage WRITE setCopyAndRenameMessage NOTIFY copyAndRenameMessageChanged)

public:
    explicit ModelSet(QObject *parent = nullptr);
    CritterModel *critterModel() const;
    PhotoModel *photoModel() const;
    NameModel *cameraModel() const;
    NameModel *transectModel() const;
    NameModel *subtransectModel() const;
    NameModel *sdcardModel() const;
    void initalizeModels();

    QString transect()  const;
    QString subtransect() const;
    QString camera() const;
    QString sdcard() const;
    QString analyst() const;
    QString copyAndRenameMessage() const;
    void setTransect(QString transect);
    void setSubtransect(QString subtransect);
    void setCamera(QString camera);
    void setSdcard(QString sdcard);
    void setAnalyst(QString analyst);
    void setCopyAndRenameMessage(const QString &copyAndRenameMessage);

    Q_INVOKABLE void copyAndRename(const QString &fromDirectory, const QString &toDirectory);
    Q_INVOKABLE bool writeXml(const QString &fromDirectory);

signals:
    void transectChanged();
    void subtransectChanged();
    void cameraChanged();
    void sdcardChanged();
    void analystChanged();
    void copyAndRenameMessageChanged();

private:
    QString m_transect;
    QString m_subtransect;
    QString m_camera;
    QString m_sdcard;
    QString m_analyst;
    QString m_copyAndRenameMessage;
    CritterModel *m_critterModel;
    PhotoModel *m_photoModel;
    NameModel  *m_cameraModel;
    NameModel  *m_transectModel;
    NameModel  *m_subtransectModel;
    NameModel  *m_sdcardModel;

    QString makeFileName(int modelIndex, int speciesIndex);
    QString makeDataLine(int modelIndex, int speciesIndex);


};

#endif // MODELSET_H
