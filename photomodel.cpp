/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */
#include <QDateTime>
#include "photomodel.h"
#include <QDebug>

Photo::Photo(QObject *parent) :
    QObject(parent),
    m_fileName(""), m_photoDate(""),
    m_oneType("None"), m_oneCount(0), m_oneDup(0), m_oneConf("High"), m_oneReqChk(false),
    m_twoType("None"), m_twoCount(0), m_twoDup(0), m_twoConf("High"), m_twoReqChk(false)
{ }

Photo::Photo(const QString &fileName, const QString &photoDate,
             const QString &oneType, const int &oneCount, const int &oneDup, const QString &oneConf, const bool oneReqChk,
             const QString &twoType, const int &twoCount, const int &twoDup, const QString &twoConf, const bool twoReqChk,
             QObject *parent)
    : QObject(parent),
    m_fileName(fileName), m_photoDate(photoDate),
    m_oneType(oneType), m_oneCount(oneCount), m_oneDup(oneDup), m_oneConf(oneConf), m_oneReqChk(oneReqChk),
    m_twoType(twoType), m_twoCount(twoCount), m_twoDup(twoDup), m_twoConf(twoConf), m_twoReqChk(twoReqChk)
{ }

QString Photo::fileName() const
{
    return m_fileName;
}

QString Photo::photoDate() const
{
    return m_photoDate;
}

QString Photo::oneType() const
{
    return m_oneType;
}

int Photo::oneCount() const
{
    return m_oneCount;
}

int Photo::oneDup() const
{
    return m_oneDup;
}

QString Photo::oneConf() const
{
    return m_oneConf;
}

bool Photo::oneReqChk() const
{
    return m_oneReqChk;
}

QString Photo::twoType() const
{
    return m_twoType;
}

int Photo::twoCount() const
{
    return m_twoCount;
}

int Photo::twoDup() const
{
    return m_twoDup;
}

QString Photo::twoConf() const
{
    return m_twoConf;
}

bool Photo::twoReqChk() const
{
    return m_twoReqChk;
}

void Photo::setFileName(const QString &fileName)
{
    m_fileName = fileName;
    emit fileNameChanged();
}

void Photo::setPhotoDate(const QString &photoDate)
{
    m_photoDate = photoDate;
    emit photoDateChanged();
}

void Photo::setOneType(const QString &oneType)
{
    m_oneType = oneType;
    emit oneTypeChanged();
}

void Photo::setOneCount(int oneCount)
{
    m_oneCount = oneCount;
    emit oneCountChanged();
}

void Photo::setOneDup(int oneDup)
{
    m_oneDup = oneDup;
    emit oneDupChanged();
}

void Photo::setOneConf(const QString &oneConf)
{
    m_oneConf = oneConf;
    emit oneConfChanged();
}

void Photo::setoneReqChk(const bool oneReqChk)
{
    m_oneReqChk = oneReqChk;
    emit oneReqChkChanged();
}

void Photo::setTwoType(const QString &twoType)
{
    m_twoType = twoType;
    emit twoTypeChanged();
}

void Photo::setTwoCount(int twoCount)
{
    m_twoCount = twoCount;
    emit twoCountChanged();
}

void Photo::setTwoDup(int twoDup)
{
    m_twoDup = twoDup;
    emit twoDupChanged();
}

void Photo::setTwoConf(const QString &twoConf)
{
    m_twoConf = twoConf;
    emit twoConfChanged();
}

void Photo::setTwoReqChk(const bool twoReqChk)
{
    m_twoReqChk = twoReqChk;
    emit twoReqChkChanged();
}


PhotoModel::PhotoModel(QObject *parent) :
    QAbstractListModel(parent), m_currentIndex(-1), m_description("")
{
}

void PhotoModel::addPhoto()
{
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    m_photos << (new Photo());
    endInsertRows();
    emit countChanged(this->rowCount());
}

void PhotoModel::clear()
{
    beginResetModel();
    // call delete on each elementin list
    qDeleteAll(m_photos.begin(), m_photos.end());
    m_photos.clear();
    endResetModel();
    emit countChanged(this->rowCount());
}

void PhotoModel::setRowData(int row, const QString &role, const QVariant &value)
{
    Photo *photo = m_photos[row];
    if (role == "fileName")
        photo->setFileName(value.toString());
    else if (role == "photoDate")
        photo->setPhotoDate(value.toString());
    else if (role == "oneType")
        photo->setOneType(value.toString());
    else if (role == "oneConf")
        photo->setOneConf(value.toString());
    else if (role == "oneCount")
        photo->setOneCount(value.toInt());
    else if (role == "oneDup")
        photo->setOneDup(value.toInt());
    else if (role == "oneReqChk")
        photo->setoneReqChk(value.toBool());
    else if (role == "twoType")
        photo->setTwoType(value.toString());
    else if (role == "twoConf")
        photo->setTwoConf(value.toString());
    else if (role == "twoCount")
        photo->setTwoCount(value.toInt());
    else if (role == "twoDup")
        photo->setTwoDup(value.toInt());
    else if (role == "twoReqChk")
        photo->setTwoReqChk(value.toBool());
    emit dataChanged(index(row), index(row));
}

QVariant PhotoModel::getRowData(int row, const QString &role)
{
    Photo *photo = m_photos[row];
    if (role == "fileName")
        return photo->fileName();
    else if (role == "photoDate")
        return photo->photoDate();
    else if (role == "oneType")
        return photo->oneType();
    else if (role == "oneConf")
        return photo->oneConf();
    else if (role == "oneCount")
        return photo->oneCount();
    else if (role == "oneDup")
        return photo->oneDup();
    else if (role == "oneReqChk")
        return photo->oneReqChk();
    else if (role == "twoType")
        return photo->twoType();
    else if (role == "twoConf")
        return photo->twoConf();
    else if (role == "twoCount")
        return photo->twoCount();
    else if (role == "twoDup")
        return photo->twoDup();
    else if (role == "twoReqChk")
        return photo->twoReqChk();
    return QVariant();
}

int PhotoModel::rowCount(const QModelIndex & parent ) const
{
   Q_UNUSED(parent);
    return m_photos.count();
}

QVariant PhotoModel::data(const QModelIndex &index, int role) const
{
    if (index.row() < 0 || index.row() >= m_photos.count())
        return QVariant();

    Photo *photo = m_photos[index.row()];
    if (role == FileNameRole)
        return photo->fileName();
    else if (role == PhotoDateRole)
        return photo->photoDate();
    else if (role == OneTypeRole)
        return photo->oneType();
    else if (role == OneConfRole)
        return photo->oneConf();
    else if (role == OneCountRole)
        return photo->oneCount();
    else if (role == OneDupRole)
        return photo->oneDup();
    else if (role == OneReqCheckRole)
        return photo->oneReqChk();
    else if (role == TwoTypeRole)
        return photo->twoType();
    else if (role == TwoConfRole)
        return photo->twoConf();
    else if (role == TwoCountRole)
        return photo->twoCount();
    else if (role == TwoDupRole)
        return photo->twoDup();
    else if (role == TwoReqCheckRole)
        return photo->twoReqChk();
    return QVariant();
}

Photo* PhotoModel::photo(int index)
{
    return m_photos[index];
}

int PhotoModel::currentIndex() const
{
    return m_currentIndex;
}

void PhotoModel::setCurrentIndex(int currentIndex)
{
    m_currentIndex = currentIndex;
    emit currentIndexChanged(m_currentIndex);
    setDescription(QString("%1     %2/%3    %4        %5")
                   .arg(timeBetween(currentIndex - 1, currentIndex))
                   .arg(currentIndex + 1, 20)
                   .arg(m_photos.count(), 2)
                   .arg(critterInfo(currentIndex))
                   .arg(timeBetween(currentIndex, currentIndex + 1), 20));
}

QString PhotoModel::description() const
{
    return m_description;
}

void PhotoModel::setDescription(QString description)
{
    m_description = description;
    emit descriptionChanged(m_description);
}

QHash<int, QByteArray> PhotoModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[FileNameRole]    = "fileName";
    roles[PhotoDateRole]   = "photoDate";
    roles[OneTypeRole]     = "oneType";
    roles[OneConfRole]     = "oneConf";
    roles[OneCountRole]    = "oneCount";
    roles[OneDupRole]      = "oneDup";
    roles[OneReqCheckRole] = "oneChkReq";
    roles[TwoTypeRole]     = "twoType";
    roles[TwoConfRole]     = "twoConf";
    roles[TwoCountRole]    = "twoCount";
    roles[TwoDupRole]      = "twoDup";
    roles[TwoReqCheckRole] = "twoChkReq";
    return roles;
}


QString PhotoModel::timeBetween(int i, int j)
{
    if( (i < 0) || (j > (m_photos.count() - 1))) {
        return "   :::   ";
    } else {
        QDateTime dtStart = getRowData(i, "photoDate").toDateTime();
        QDateTime dtEnd = getRowData(j, "photoDate").toDateTime();
        qint64 seconds = dtStart.secsTo(dtEnd);
        long hour, min, sec;
        hour = seconds/3600;
        min = (seconds%3600) / 60;
        sec = (seconds%3600) % 60;
        return QString("%1:%2:%3")
                .arg(hour, 3, 10, QLatin1Char(' '))
                .arg(min, 2, 10, QLatin1Char('0'))
                .arg(sec, 2, 10, QLatin1Char('0'));
    }
}

QString PhotoModel::critterInfo(int i)
{
    if((i < 0) || i > (m_photos.count() - 1)) {
        return "";
    } else {
        QString oneType = getRowData(i, "oneType").toString();
        int oneCount = getRowData(i, "oneCount").toInt();
        int oneDup = getRowData(i, "oneDup").toInt();
        QString oneConf = getRowData(i, "oneConf").toString().left(1);
        QString twoType = getRowData(i, "twoType").toString();
        int twoCount = getRowData(i, "twoCount").toInt();
        int twoDup = getRowData(i, "twoDup").toInt();
        QString twoConf = getRowData(i, "twoConf").toString().left(1);

        return QString("%1 (%2,%4, %5)  %6 (%7,%8, %9)")
                .arg(oneType, 25)
                .arg(oneCount, 3, 10, QLatin1Char(' '))
                .arg(oneDup, 3, 10, QLatin1Char(' '))
                .arg(oneConf)
                .arg(twoType, 25)
                .arg(twoCount, 3, 10, QLatin1Char(' '))
                .arg(twoDup, 3, 10, QLatin1Char(' '))
                .arg(twoConf);

    }
}
