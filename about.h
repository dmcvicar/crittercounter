/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#ifndef ABOUT_H
#define ABOUT_H

#include <QObject>

class About: public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString help           READ help           WRITE setHelp           NOTIFY helpChanged)
    Q_PROPERTY(QString licence        READ licence        WRITE setLicence        NOTIFY licenceChanged)
    Q_PROPERTY(QString acknowledgment READ acknowledgment WRITE setAcknowledgment NOTIFY acknowledgmentChanged)

public:
    About(QObject *parent = nullptr);

    QString help()  const;
    QString licence() const;
    QString acknowledgment() const;
    void setHelp(const QString &help);
    void setLicence(const QString &licence);
    void setAcknowledgment(const QString &acknowledgment);

signals:
    void helpChanged();
    void licenceChanged();
    void acknowledgmentChanged();

private:
    QString m_help;
    QString m_licence;
    QString m_acknowledgment;
};

#endif // ABOUT_H
