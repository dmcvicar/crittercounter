/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.3
import Qt.labs.folderlistmodel 2.1
import McVicar.QExif 1.0

Window {
    id: root
    visible: true
    width: 1024
    height: 600
    color: "#E0E0E0"
    title: qsTr("Critter Counter Ver-0.02")

    QExif {
        id: qexif
    }

    MessageDialog {
        id: noFolderMessage
        title: "No Folder Selected"
        icon: StandardIcon.Information
        text: "No download photo folder selected"
        standardButtons: StandardButton.Ok
    }

    MessageDialog {
        id: transectMessage
        title: "Missing Transect Information"
        icon: StandardIcon.Information
        text: "Missing Transect Information"
        standardButtons: StandardButton.Ok
    }

    MessageDialog {
        id: uploadMessage
        title:  "Upload Message"
        icon: StandardIcon.Information
        text: controler.copyAndRenameMessage
        standardButtons: StandardButton.Ok
    }

    FileDialog {
        id: downloadDirectoryDialog
        title: "Choose a folder with images to process"
        selectFolder: true
        folder: picturesLocation
        onAccepted: {
            folderModel.folder = folder
            root.title = folder.toString().substring(8)
        }
    }

    FileDialog {
        id: uploadDirectoryDialog
        title: "Choose a folder to store renamed images"
        selectFolder: true
        folder: downloadDirectoryDialog.folder
        onAccepted: {
            controler.copyAndRename(downloadDirectoryDialog.folder, uploadDirectoryDialog.folder)
            controler.writeXml(downloadDirectoryDialog.folder)
            uploadMessage.open()
        }
    }

    Dialog {
        id: transectDialog
        title: "Transect Information"
        standardButtons: StandardButton.Save | StandardButton.Cancel
        onAccepted: {
            controler.camera = cameraComboBox.currentText
            controler.sdcard = sdcardComboBox.currentText
            controler.transect = transectComboBox.currentText
            controler.subtransect = subtransectComboBox.currentText
            controler.analyst = analystTextEdit.text;
        }
        GridLayout {
            id: transectGrid
            Layout.fillWidth: true
            columns: 2
            Text { text: "Camera"; font.bold: true }
            ComboBox {
                id: cameraComboBox
                model: cameraModel
                textRole: "name"
                Layout.minimumWidth: 100
            }
            Text { text: "SD Card"; font.bold: true }
            ComboBox {
                id: sdcardComboBox
                model: sdcardModel
                textRole: "name"
                Layout.minimumWidth: 100
            }
            Text { text: "Transect"; font.bold: true; }
            ComboBox {
                id: transectComboBox
                model: transectModel
                textRole: "name"
                Layout.minimumWidth: 100
            }
            Text { text: "Sub Transect"; font.bold: true }
            ComboBox {
                id: subtransectComboBox
                model: subtransectModel
                textRole: "name"
                Layout.minimumWidth: 100
            }
            Text { text: "Analyst"; font.bold: true }
            TextField {
                id: analystTextEdit
                Layout.minimumWidth: 200
            }
        }
    }

    Dialog {
        id: crittersDialog
        title: "Critter Count"
        standardButtons: StandardButton.Save | StandardButton.Cancel
        onAccepted: {
            photoModel.setRowData(photoview.currentIndex, "oneType", species1Type.currentText)
            photoModel.setRowData(photoview.currentIndex, "oneConf", species1Conf.currentText)
            photoModel.setRowData(photoview.currentIndex, "oneCount", species1Count.value)
            photoModel.setRowData(photoview.currentIndex, "oneDup", species1DupCount.value)
            photoModel.setRowData(photoview.currentIndex, "oneReqChk", species1ReqChk.checked)
            photoModel.setRowData(photoview.currentIndex, "twoType", species2Type.currentText)
            photoModel.setRowData(photoview.currentIndex, "twoConf", species2Conf.currentText)
            photoModel.setRowData(photoview.currentIndex, "twoCount", species2Count.value)
            photoModel.setRowData(photoview.currentIndex, "twoDup", species2DupCount.value)
            photoModel.setRowData(photoview.currentIndex, "twoReqChk", species2ReqChk.checked)
            photoModel.currentIndex = photoview.currentIndex; // sloppy ugly
        }
        GridLayout {
            columns: 3
            Layout.fillWidth: true
            Text { text: "" }
            Text { text: "Species 1";    font.bold: true; }
            Text { text: "Species 2";    font.bold: true; }
            Text { text: "Species Name"; font.bold: true; }
            ComboBox {
                id: species1Type
                model: critterModel
                textRole: "name"
                Layout.minimumWidth: 125
            }
            ComboBox {
                id: species2Type
                model: critterModel
                textRole: "name"
                Layout.minimumWidth: 125
            }

            Text { text: "Confidence";   font.bold: true; }
            ComboBox {
                id: species1Conf
                model: [ "High", "Moderate", "Low" ]
                Layout.minimumWidth: 125
            }
            ComboBox {
                id: species2Conf
                model: [ "High", "Moderate", "Low" ]
                Layout.minimumWidth: 125
            }

            Text { text: "Count Total"; font.bold: true; }
            SpinBox {
                id: species1Count
            }
            SpinBox {
                id: species2Count
            }
            Text { text: "Count Duplicate"; font.bold: true; }
            SpinBox {
                id: species1DupCount
            }
            SpinBox {
                id: species2DupCount
            }
            Text { text: "Request Check"; font.bold: true; }
            CheckBox {
                id: species1ReqChk
                }
            CheckBox {
                id: species2ReqChk
                }
            Text { text: ""}
        }
    }

    Dialog {
        id: informationDialog
        standardButtons: StandardButton.Ok
        width: 800
        height: 400
        title: "Internal Model"
        TableView {
            anchors.fill: parent
            model: photoModel
            TableViewColumn {
                role: "fileName"
                title: "fileName"
                width: 120
            }
            TableViewColumn {
                role: "photoDate"
                title: "photoDate"
                width: 150
            }
            TableViewColumn {
                role: "oneType"
                title: "oneType"
                width: 120
            }
            TableViewColumn {
                role: "oneConf"
                title: "oneConf"
                width: 120
            }
            TableViewColumn {
                role: "oneCount"
                title: "oneCount"
                width: 80
            }
            TableViewColumn {
                role: "oneDup"
                title: "oneDup"
                width: 80
            }
            TableViewColumn {
                role: "twoType"
                title: "twoType"
                width: 120
            }
            TableViewColumn {
                role: "twoConf"
                title: "twoConf"
                width: 120
            }
            TableViewColumn {
                role: "twoCount"
                title: "twoCount"
                width: 80
            }
            TableViewColumn {
                role: "twoDup"
                title: "twoDup"
                width: 80
            }
        }
    }

    Dialog {
        id: aboutDialog
        standardButtons: StandardButton.Ok
        width: 800
        height: 400
        title: "About"
        TabView {
            anchors.fill: parent
            Tab {
                title: "Help"
                TextArea {
                    text: about.help
                    textFormat: TextEdit.RichText
                    readOnly: true
                }
            }
            Tab {
                title: "Licence"
                TextArea {
                    text: about.licence
                    textFormat: TextEdit.RichText
                    readOnly: true
                }
            }
            Tab {
                title: "Acknowledgment"
                TextArea {
                    text: about.acknowledgment
                    textFormat: TextEdit.RichText
                    readOnly: true
                }
            }
        }
    }

    ListView {
        id: photoview
        x: 0
        y: 5
        height: parent.height - 25
        width: parent.width

        focus: true
        Keys.onLeftPressed: {
            if (photoview.currentIndex > 0) {
                photoview.currentIndex = photoview.currentIndex - 1
            }
        }
        Keys.onRightPressed: {
            if (photoview.currentIndex < (photoview.count - 1)) {
                photoview.currentIndex = photoview.currentIndex + 1
            }
        }

        model: FolderListModel {
            id: folderModel
            objectName: "folderModel"
            sortField :  "Name"
            showDirs: false
            nameFilters: ["*.jpg", "*.JPG"]

            onFolderChanged: {
                photoview.currentIndex = 0
                photoModel.clear()
                for (var i = 0; i < folderModel.count; i++) {
                    qexif.fileName = folderModel.get(i,"fileURL")
                    photoModel.addPhoto()
                    photoModel.setRowData(i, "fileName", folderModel.get(i,"fileName"))
                    photoModel.setRowData(i, "photoDate", qexif.creationDate)
                    photoModel.setRowData(i, "oneType", critterModel.getRowData(0, "name"))
                    photoModel.setRowData(i, "oneCount", 0)
                    photoModel.setRowData(i, "oneDup", 0)
                    photoModel.setRowData(i, "twoType", critterModel.getRowData(0, "name"))
                    photoModel.setRowData(i, "twoCount", 0)
                    photoModel.setRowData(i, "twoDup", 0)
                }
                controler.transect = transectModel.getRowData(0, "name")
                controler.subtransect = subtransectModel.getRowData(0, "name")
                controler.camera = cameraModel.getRowData(0, "name")
                photoModel.currentIndex = photoview.currentIndex
            }
        }
        delegate: Image {
            source: fileURL
            width: photoview.width
            height: photoview.height
            sourceSize.height: height
            sourceSize.width: width
            fillMode: Image.PreserveAspectFit
            asynchronous: true
           // smooth: true // do i need both smooth and mipmap
            mipmap: true // or do they conflict ??
        }
        orientation: ListView.Horizontal
        snapMode: ListView.SnapToItem
        cacheBuffer: width * 4
        highlightRangeMode: ListView.StrictlyEnforceRange
        onCurrentIndexChanged: {
            photoModel.currentIndex = photoview.currentIndex
        }
    }

    Text {
        x: 0
        y: parent.height - 20
        height: 20
        width: parent.width
        text: photoModel.description
        font.family: "Helvetica"
        font.pointSize: 15
        color: "black"
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    ColumnLayout {
        x: 5
        y: 0
        width: 100
        height: root.height
        Layout.fillHeight : true
        Layout.fillWidth: true


        MyButton {

            upImage: "resources/download_up_128.png"
            downImage: "resources/download_dwn_128.png"
            toolTip: "Open an image directory containing files to be processed"
            onClicked: downloadDirectoryDialog.open()
        }
        MyButton {
            upImage: "resources/transect_up_128.png"
            downImage: "resources/transect_dwn_128.png"
            toolTip: "Set Transect Information for opened directory"
            onClicked: {
                if (folderModel.count > 0) {
                    cameraComboBox.currentIndex =  cameraComboBox.find(controler.camera)
                    sdcardComboBox.currentIndex = sdcardComboBox.find(controler.sdcard)
                    transectComboBox.currentIndex =  transectComboBox.find(controler.transect)
                    subtransectComboBox.currentIndex =  subtransectComboBox.find(controler.subtransect)
                    analystTextEdit.text = controler.analyst
                    transectDialog.open()
                } else {
                    noFolderMessage.open()
                }
            }
        }
        MyButton {
            upImage: "resources/critter_up_128.png"
            downImage: "resources/critter_dwn_128.png"
            toolTip: "Set Critter Information for current photo"
            onClicked:
                if (folderModel.count > 0) {
                    // update  critter dialog control values for current immage from model
                    species1Type.currentIndex =  species1Type.find(photoModel.getRowData(photoview.currentIndex, "oneType"))
                    species1Count.value = photoModel.getRowData(photoview.currentIndex, "oneCount")
                    species1DupCount.value = photoModel.getRowData(photoview.currentIndex, "oneDup")
                    species1Conf.currentIndex = species1Conf.find(photoModel.getRowData(photoview.currentIndex, "oneConf"))
                    species1ReqChk.checked = photoModel.getRowData(photoview.currentIndex, "oneReqChk")
                    species2Type.currentIndex =  species2Type.find(photoModel.getRowData(photoview.currentIndex, "twoType"))
                    species2Count.value = photoModel.getRowData(photoview.currentIndex, "twoCount")
                    species2DupCount.value = photoModel.getRowData(photoview.currentIndex, "twoDup")
                    species2Conf.currentIndex = species2Conf.find(photoModel.getRowData(photoview.currentIndex, "twoConf"))
                    species2ReqChk.checked = photoModel.getRowData(photoview.currentIndex, "twoReqChk")
                    crittersDialog.open()
                } else {
                    noFolderMessage.open()
                }
        }
        MyButton {
            upImage: "resources/model_up_128.png"
            downImage: "resources/model_dwn_128.png"
            toolTip: "Display photo model"
            onClicked: {
                informationDialog.open()
            }
        }
        MyButton {
            upImage: "resources/upload_up_128.png"
            downImage: "resources/upload_dwn_128.png"
            toolTip: "Upload renamed image files to specified directory"
            onClicked: {
                if (folderModel.count > 0) {
                    if (controler.transect === transectModel.getRowData(0, "name")) {
                        transectMessage.text = "Error: Transect name not defined\n\n" +
                                "Transect name has to be defined prior to uploading files"
                        transectMessage.open()
                    } else if (controler.subtransect === subtransectModel.getRowData(0, "name")) {
                        transectMessage.text = "Error: Sub Transect name not defined\n\n" +
                                "Sub Transect name has to be defined prior to uploading files"
                        transectMessage.open()
                    } else {
                        uploadDirectoryDialog.open()
                    }
                } else {
                    noFolderMessage.open()
                }
            }
        }
        MyButton {
            upImage: "resources/settings_up_128.png"
            downImage: "resources/settings_dwn_128.png"
            toolTip: "Settings"
            //onClicked: aboutDialog.open()
        }
        MyButton {
            upImage: "resources/about_up_128.png"
            downImage: "resources/about_dwn_128.png"
            toolTip: "Information about this program"
            onClicked: aboutDialog.open()
        }
    }
    // location should be resolved based on os
    property string picturesLocation: "file:///Users"
}

