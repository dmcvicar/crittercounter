/*
 * CritterCounter -- A program for identifing and counting species from JPG images.
 *
 * This file is part of the CritterCounter program's source code.
 *
 * Copyright (c) 2017, 2018 David McVicar dmcvicar@telus.net
 * All rights reserved (openBSD License).
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <QUrl>
#include <QDir>
#include <QDateTime>
#include "modelset.h"

#include <QDebug>

ModelSet::ModelSet(QObject *parent) : QObject(parent),
    m_transect(""), m_subtransect(""), m_camera(""), m_sdcard(""), m_analyst(""),
    m_critterModel(new CritterModel()),
    m_photoModel(new PhotoModel()), m_cameraModel(new NameModel()),
    m_transectModel(new NameModel()), m_subtransectModel(new NameModel()),
    m_sdcardModel(new NameModel())
{
    initalizeModels();
}

CritterModel *ModelSet::critterModel() const
{
    return m_critterModel;
}

PhotoModel *ModelSet::photoModel() const
{
    return m_photoModel;
}

NameModel *ModelSet::cameraModel() const
{
    return m_cameraModel;
}

NameModel *ModelSet::transectModel() const
{
    return m_transectModel;
}

NameModel *ModelSet::subtransectModel() const
{
    return m_subtransectModel;
}

NameModel *ModelSet::sdcardModel() const
{
    return m_sdcardModel;
}

// the initialization should read from configuration files at some point
void ModelSet::initalizeModels()
{
    m_critterModel->addCritter(new Critter("None",         "__"));
    m_critterModel->addCritter(new Critter("Bobcat",       "bc"));
    m_critterModel->addCritter(new Critter("Cougar",       "cu"));
    m_critterModel->addCritter(new Critter("Coyote",       "cy"));
    m_critterModel->addCritter(new Critter("Domestic Cat", "dc"));
    m_critterModel->addCritter(new Critter("Domestic Dog", "dd"));
    m_critterModel->addCritter(new Critter("Elk",          "ek"));
    m_critterModel->addCritter(new Critter("Fisher",       "fi"));
    m_critterModel->addCritter(new Critter("Gray Wolf",    "gw"));
    m_critterModel->addCritter(new Critter("Hare",         "ha"));
    m_critterModel->addCritter(new Critter("Human",        "hu"));
    m_critterModel->addCritter(new Critter("Lynx",         "ly"));
    m_critterModel->addCritter(new Critter("Marten",       "ma"));
    m_critterModel->addCritter(new Critter("Mink",         "mi"));
    m_critterModel->addCritter(new Critter("Moose",        "mo"));
    m_critterModel->addCritter(new Critter("Mouse",        "mu"));
    m_critterModel->addCritter(new Critter("Mule Deer",    "md"));
    m_critterModel->addCritter(new Critter("Muskrat",      "mr"));
    m_critterModel->addCritter(new Critter("Red Fox",      "rf"));
    m_critterModel->addCritter(new Critter("Red Squirrel", "rs"));
    m_critterModel->addCritter(new Critter("Shrew",        "sh"));
    m_critterModel->addCritter(new Critter("Vole",         "vo"));
    m_critterModel->addCritter(new Critter("Weasel",       "we"));
    m_critterModel->addCritter(new Critter("White Tail Deer", "wt"));
    m_critterModel->addCritter(new Critter("Unknown",      "un"));

    m_cameraModel->addName(new Name("Un"));
    m_cameraModel->addName(new Name("1"));
    m_cameraModel->addName(new Name("2"));
    m_cameraModel->addName(new Name("3"));
    m_cameraModel->addName(new Name("4"));
    m_cameraModel->addName(new Name("5"));
    m_cameraModel->addName(new Name("6"));
    m_cameraModel->addName(new Name("7"));
    m_cameraModel->addName(new Name("8"));
    m_cameraModel->addName(new Name("9"));
    m_cameraModel->addName(new Name("10"));
    m_cameraModel->addName(new Name("11"));
    m_cameraModel->addName(new Name("12"));
    m_cameraModel->addName(new Name("13"));
    m_cameraModel->addName(new Name("14"));
    m_cameraModel->addName(new Name("15"));
    m_cameraModel->addName(new Name("16"));
    m_cameraModel->addName(new Name("17"));
    m_cameraModel->addName(new Name("18"));
    m_cameraModel->addName(new Name("19"));
    m_cameraModel->addName(new Name("20"));
    m_cameraModel->addName(new Name("21"));
    m_cameraModel->addName(new Name("22"));
    m_cameraModel->addName(new Name("23"));
    m_cameraModel->addName(new Name("24"));
    m_cameraModel->addName(new Name("25"));
    m_cameraModel->addName(new Name("26"));
    m_cameraModel->addName(new Name("27"));
    m_cameraModel->addName(new Name("C1"));
    m_cameraModel->addName(new Name("C2"));
    m_cameraModel->addName(new Name("C3"));

    m_sdcardModel->addName(new Name("Un"));
    m_sdcardModel->addName(new Name("4"));
    m_sdcardModel->addName(new Name("5"));
    m_sdcardModel->addName(new Name("5-B"));
    m_sdcardModel->addName(new Name("6"));
    m_sdcardModel->addName(new Name("6-B"));
    m_sdcardModel->addName(new Name("7"));
    m_sdcardModel->addName(new Name("7-B"));
    m_sdcardModel->addName(new Name("8"));
    m_sdcardModel->addName(new Name("9"));
    m_sdcardModel->addName(new Name("10"));
    m_sdcardModel->addName(new Name("11"));
    m_sdcardModel->addName(new Name("11-B"));
    m_sdcardModel->addName(new Name("12-B"));
    m_sdcardModel->addName(new Name("14"));
    m_sdcardModel->addName(new Name("16-B"));
    m_sdcardModel->addName(new Name("17-B"));
    m_sdcardModel->addName(new Name("18-B"));
    m_sdcardModel->addName(new Name("19"));
    m_sdcardModel->addName(new Name("20"));
    m_sdcardModel->addName(new Name("20-B"));
    m_sdcardModel->addName(new Name("21"));
    m_sdcardModel->addName(new Name("22-B"));
    m_sdcardModel->addName(new Name("22-B1"));
    m_sdcardModel->addName(new Name("X1"));
    m_sdcardModel->addName(new Name("X41"));
    m_sdcardModel->addName(new Name("X5-1"));



    
    m_transectModel->addName(new Name("??"));
    m_transectModel->addName(new Name("1"));
    m_transectModel->addName(new Name("2"));
    m_transectModel->addName(new Name("3"));
    m_transectModel->addName(new Name("4"));
    m_transectModel->addName(new Name("5"));
    m_transectModel->addName(new Name("6"));
    m_transectModel->addName(new Name("7"));
    m_transectModel->addName(new Name("8"));
    m_transectModel->addName(new Name("9"));
    m_transectModel->addName(new Name("10"));
    
    m_subtransectModel->addName(new Name("??"));
    m_subtransectModel->addName(new Name("1"));
    m_subtransectModel->addName(new Name("2"));
    m_subtransectModel->addName(new Name("3"));
    m_subtransectModel->addName(new Name("4"));
    m_subtransectModel->addName(new Name("5"));
    m_subtransectModel->addName(new Name("6"));
    m_subtransectModel->addName(new Name("7"));
    m_subtransectModel->addName(new Name("8"));
    m_subtransectModel->addName(new Name("9"));
    m_subtransectModel->addName(new Name("10"));
    

}

QString ModelSet::transect() const
{
    return m_transect;
}

QString ModelSet::subtransect() const
{
    return m_subtransect;
}

QString ModelSet::camera() const
{
    return m_camera;
}

QString ModelSet::sdcard() const
{
    return m_sdcard;
}

QString ModelSet::analyst() const
{
    return m_analyst;
}

QString ModelSet::copyAndRenameMessage() const
{
    return m_copyAndRenameMessage;
}

void ModelSet::setTransect(QString transect)
{
    m_transect = transect;
    emit transectChanged();
}

void ModelSet::setSubtransect(QString subtransect)
{
    m_subtransect = subtransect;
    emit subtransectChanged();
}

void ModelSet::setCamera(QString camera)
{
    m_camera = camera;
    emit cameraChanged();
}

void ModelSet::setSdcard(QString sdcard)
{
    m_sdcard = sdcard;
    emit sdcardChanged();
}

void ModelSet::setAnalyst(QString analyst)
{
    m_analyst = analyst;
    emit analystChanged();
}

void ModelSet::setCopyAndRenameMessage(const QString &copyAndRenameMessage)
{
    m_copyAndRenameMessage = copyAndRenameMessage;
    emit copyAndRenameMessageChanged();
}


void ModelSet::copyAndRename(const QString &fromDirectory, const QString &toDirectory)
{
    if (fromDirectory == toDirectory) {
        setCopyAndRenameMessage("Error: same upload directory as download directory\n\n "
                                "These directories have to be distinct");
        return;
    }

    QDir fromDir(QUrl(fromDirectory).toLocalFile());
    QDir toDir(QUrl(toDirectory).toLocalFile());
    if (!toDir.isEmpty()) {
        setCopyAndRenameMessage("Upload directory must start out empty\n\n"
                                "Either select a new directory or clear the contents of the chosen directory");
        return;
    }

    QFile summaryFile(toDir.absoluteFilePath("__data_summary.csv"));
    if (!summaryFile.open(QIODevice::WriteOnly)) {
        setCopyAndRenameMessage(" Unable to create data summary file");
        return;
    } else {
        QTextStream stream(&summaryFile);
        stream << "Date, Camera, SD_Card, Transect, Sub_Transect, Species_Code, Confidence, Count, Duplicates, Total, Photo, Analyist, Request_Check" << endl;
        for (int i = 0; i < m_photoModel->rowCount(); i++) {
            Photo* photo = m_photoModel->photo(i);
            QFile file(fromDir.absoluteFilePath(photo->fileName()));
            QString fileName;
            for (int j = 0; j <= 1; j++) {
                fileName = makeFileName(i, j);
                if (!fileName.isEmpty()) {
                    stream << makeDataLine(i, j) << endl;
                    file.copy(toDir.absoluteFilePath(fileName));
                }
            }
        }
        summaryFile.close();
    }
    setCopyAndRenameMessage("Successful copy and rename");
}

bool ModelSet::writeXml(const QString &fromDirectory)
{
    qDebug() << "fromDirectory: " << fromDirectory;
    QDir directory(QUrl(fromDirectory).toLocalFile());
    QString fileName = directory.absoluteFilePath("CitterCounter.xml");
    qDebug() << "file: " << fileName;
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        qDebug() << "file error";
        return false;
    }
    QXmlStreamWriter xmlWriter(&file);
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("CritterCounter");
    xmlWriter.writeAttribute("Version", "1.0");
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement(QStringLiteral("TransectModel"));
    xmlWriter.writeAttribute(QStringLiteral("Camera"), m_camera);
    xmlWriter.writeAttribute(QStringLiteral("SDCard"), m_sdcard);
    xmlWriter.writeAttribute(QStringLiteral("Transect"), m_transect);
    xmlWriter.writeAttribute(QStringLiteral("SubTransect"), m_subtransect);
    xmlWriter.writeAttribute(QStringLiteral("Analyst"), m_analyst);
    xmlWriter.writeEndElement();
    xmlWriter.writeStartElement(QStringLiteral("PhotoModel"));
    for(int i = 0; i < m_photoModel->rowCount(); i++) {
        xmlWriter.writeStartElement(QString("Photo_%1").arg(i));
        xmlWriter.writeTextElement("fromDirectory", fromDirectory);
        xmlWriter.writeTextElement("fileName", m_photoModel->getRowData(i,"fileName").toString());
        xmlWriter.writeTextElement("photoDate",  m_photoModel->getRowData(i,"photoDate").toString());
        xmlWriter.writeTextElement("oneType", m_photoModel->getRowData(i,"oneType").toString());
        xmlWriter.writeTextElement("oneConf", m_photoModel->getRowData(i,"oneConf").toString());
        xmlWriter.writeTextElement("oneCount",  m_photoModel->getRowData(i,"oneCount").toString());
        xmlWriter.writeTextElement("oneDup",  m_photoModel->getRowData(i,"oneDup").toString());
        xmlWriter.writeTextElement("oneReqChk", m_photoModel->getRowData(i,"oneReqChk").toString());
        xmlWriter.writeTextElement("twoType", m_photoModel->getRowData(i,"twoType").toString());
        xmlWriter.writeTextElement("twoConf", m_photoModel->getRowData(i,"twoConf").toString());
        xmlWriter.writeTextElement("twoCount",  m_photoModel->getRowData(i,"twoCount").toString());
        xmlWriter.writeTextElement("twoDup",  m_photoModel->getRowData(i,"twoDup").toString());
        xmlWriter.writeTextElement("twoReqChk", m_photoModel->getRowData(i,"twoReqChk").toString());
        xmlWriter.writeEndElement();
    }
    file.close();
    if (file.error()) {
        return false;
    }
    return true;
}


// make a filename based on the model info
// ugly implimentation -- please rewrite
QString ModelSet::makeFileName(int modelIndex, int speciesIndex)
{
    if (m_photoModel->rowCount() > 0) {
        Photo* photo = m_photoModel->photo(modelIndex);
        if (speciesIndex == 0) {
            if (photo->oneType() != "None") {
                QString outString;
                outString = m_critterModel->symbol(photo->oneType()) + "_";
                outString += m_camera.rightJustified(2, QLatin1Char('0')) + "_";
                outString += m_transect.rightJustified(2, QLatin1Char('0')) + "_";
                outString += m_subtransect.rightJustified(2, QLatin1Char('0')) + "_";
                QDateTime dt = QDateTime::fromString(photo->photoDate(), "yyyy:MM:dd hh:mm:ss");
                outString += dt.toString("ddMMMyyyy_hhmmssap_");
                outString += QString("%1").arg(photo->oneCount(), 2, 10, QLatin1Char('0')) + "_";
                outString += QString("%1").arg(photo->oneDup(), 2, 10, QLatin1Char('0'));
                outString += ".jpg";
                if ((photo->oneType() == "Human") || (photo->twoType() == "Human")) {
                    return "DEL_" + outString;
                } else {
                    return outString;
                }
            } else {
                // no species 1
                return "";
            }
        }
        if (speciesIndex == 1) {
            if (photo->twoType() != "None") {
                QString outString;
                outString = m_critterModel->symbol(photo->twoType()) + "_";
                outString += m_camera.rightJustified(2, QLatin1Char('0')) + "_";
                outString += m_transect.rightJustified(2, QLatin1Char('0')) + "_";
                outString += m_subtransect.rightJustified(2, QLatin1Char('0')) + "_";
                QDateTime dt = QDateTime::fromString(photo->photoDate(), "yyyy:MM:dd hh:mm:ss");
                outString += dt.toString("ddMMMyyyy_hhmmssap_");
                outString += QString("%1").arg(photo->twoCount(), 2, 10, QLatin1Char('0')) + "_";
                outString += QString("%1").arg(photo->twoDup(), 2, 10, QLatin1Char('0'));
                outString += ".jpg";
                if ((photo->oneType() == "Human") || (photo->twoType() == "Human")) {
                    return "DEL_" + outString;
                } else {
                    return outString;
                }
            } else {
                //no species 2"
                return "";
            }
        }
    }
    //no photo
    return "";

}

// stream << "Date, Camera, SD_Card, Transect, Sub_Transect, Species_Code, Confidence, Count, Duplicates, Total, Photo, Analyist, Request_Check" << endl;
// still crappy ... added confidence nr request check
// ugly implimentation -- please rewrite
QString ModelSet::makeDataLine(int modelIndex, int speciesIndex)
{
    if (m_photoModel->rowCount() > 0) {
        Photo* photo = m_photoModel->photo(modelIndex);
        if (speciesIndex == 0) {
            if (photo->oneType() != "None") {
                QString outString;
                QDateTime dt = QDateTime::fromString(photo->photoDate(), "yyyy:MM:dd hh:mm:ss");
                outString += dt.toString("dd-MMM-yyyy") + ",";
                outString += "\"" + m_camera.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_sdcard + "\",";
                outString += "\"" + m_transect.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_subtransect.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_critterModel->symbol(photo->oneType()) + "\",";
                outString += QString("%1,%2,%3,%4,")
                        .arg(photo->oneConf())
                        .arg(photo->oneCount())
                        .arg(photo->oneDup())
                        .arg(photo->oneCount() - photo->oneDup());
                outString += makeFileName(modelIndex, speciesIndex);
                outString += "," + m_analyst + ",";
                if(photo->oneReqChk())
                    outString += "Please check";
                return outString;
            } else {
                // no species 1
                return "";
            }
        } else if (speciesIndex == 1) {
            if (photo->twoType() != "None") {
                QString outString;
                QDateTime dt = QDateTime::fromString(photo->photoDate(), "yyyy:MM:dd hh:mm:ss");
                outString += dt.toString("dd-MMM-yyyy") + ",";
                outString += "\"" + m_camera.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_sdcard + "\",";
                outString += "\"" + m_transect.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_subtransect.rightJustified(2, QLatin1Char('0')) + "\",";
                outString += "\"" + m_critterModel->symbol(photo->twoType()) + "\",";
                outString += QString("%1,%2,%3,%4,")
                        .arg(photo->twoConf())
                        .arg(photo->twoCount())
                        .arg(photo->twoDup())
                        .arg(photo->twoCount() - photo->twoDup());
                outString += makeFileName(modelIndex, speciesIndex);
                outString += "," + m_analyst + ",";
                if(photo->oneReqChk())
                    outString += "Please check";
                return outString;
            } else {
                // no species 1
                return "";
            }
        }
    }
    //no photo
    return "";
}



